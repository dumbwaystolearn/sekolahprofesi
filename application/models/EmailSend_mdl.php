<?php if ( ! defined('BASEPATH')) exit('Dilarang akses langsung gan?!');

class EmailSend_mdl extends CI_Model
{
	var $tbl = 'sendEmail';

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function count_all()
	{
		return $this->db->count_all($this->tbl);
	}

	function insert(){
		$id = uniqid();
		$data = array(
			'id' => $id,
			'name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'telephone' => $this->input->post('telephone'),
			'createdAt' => date('Y-m-d H:i:s')
			);
		$q = $this->db->insert($this->tbl, $data);
		if ($q){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function checkSubmit(){
		$email = $this->input->post('email');
		$telephone = $this->input->post('telephone');

		$this->db->where('email', $email);
		$this->db->or_where('telephone', $telephone);
		$q = $this->db->get($this->tbl);
		if($q->num_rows() > 0){
			return ['status'=>false, 'msg'=>'Email atau telephone sudah terdaftar'];
		}else{
			return ['status'=>true, 'msg'=>'Email atau telephone belum terdaftar'];
		}
	}

	function find(){
		$q = $this->db->get($this->tbl);
		if ($q->num_rows() > 0) {
			return array('status' => TRUE, 'result' => $q->result_array());
		}else{
			return array('status' => FALSE, 'result' => NULL);
		}
	}
}
