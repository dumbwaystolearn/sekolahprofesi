<?php if ( ! defined('BASEPATH')) exit('Dilarang akses langsung gan?!');

class Registrans_mdl extends CI_Model
{
	var $tbl = 'register';

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function count_all()
	{
		return $this->db->count_all($this->tbl);
	}

	function find(){
		$this->db->order_by('createdAt', 'asc');
		$q = $this->db->get($this->tbl);
		if ($q->num_rows() > 0) {
			return array('status' => TRUE, 'result' => $q->result_array());
		}else{
			return array('status' => FALSE, 'result' => NULL);
		}
	}

	public function checkRegistrant(){
		$email = $this->input->post('email');
		$telephone = $this->input->post('telephone');

		$this->db->where('email', $email);
		$this->db->or_where('telephone', $telephone);
		$q = $this->db->get($this->tbl);
		if($q->num_rows() > 0){
			return ['status'=>false, 'msg'=>'Email atau telephone sudah terdaftar'];
		}else{
			return ['status'=>true, 'msg'=>'Email atau telephone belum terdaftar'];
		}
	}

	function insert(){
		$id = uniqid();
		$data = array(
			'id' => $id,
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'age' => $this->input->post('age'),
			'email' => $this->input->post('email'),
			'telephone' => $this->input->post('telephone'),
			'referrer' => $this->input->post('referrer'),
			'payment_plan' => $this->input->post('payment_plan'),
			'motivation' => $this->input->post('motivation'),
			'class_schedule' => $this->input->post('class_schedule'),
			'option_register' => $this->input->post('option_register'),
			'status' => 'pending',
			'choice_profetion' => $this->input->post('choice_profetion'),
			'createdAt' => date('Y-m-d H:i:s')
			);
		$q = $this->db->insert($this->tbl, $data);
		if ($q){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	function change_status(){
		$data = array(
			'status' => $this->input->post('status'),
			);
		$this->db->where('id', $this->input->post('id'));
		$q = $this->db->update($this->tbl, $data);
		return $q;
	}

}
