<?php if ( ! defined('BASEPATH')) exit('Dilarang akses langsung gan?!');

class User_mdl extends CI_Model{
	var $tbl = 'users';

	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	function getUserByUsername($username = ''){
			$this->db->where('email', $username);
			$this->db->or_where('username', $username);
      $q = $this->db->get($this->tbl);
      if ($q->num_rows() > 0){
          return array('status' => TRUE, 'hsl' => $q->row_array());
      }else{
          return array('status'=>FALSE, 'hsl'=>NULL);
      }
	}

	function auth($username, $pass){
			$this->db->group_start();
			$this->db->where('email', $username);
			$this->db->or_where('username', $username);
			$this->db->group_start();
			$this->db->where('password', $pass);
      $q = $this->db->get($this->tbl);
      if ($q->num_rows() > 0){
          return TRUE;
      }else{
          return FALSE;
      }
	}

}
