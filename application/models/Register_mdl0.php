<?php if ( ! defined('BASEPATH')) exit('Dilarang akses langsung gan?!');

class Register_mdl extends CI_Model
{
	var $tbl = 'register';

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function count_all()
	{
		return $this->db->count_all($this->tbl);
	}

	function insert(){
		$id = uniqid();
		$data = array(
			'id' => $id,
			'full_name' => $this->input->post('fullname'),
			'age' => $this->input->post('age'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'referrer' => $this->input->post('referrer'),
			'payment_plan' => $this->input->post('paymentPlan'),
			'motivation' => $this->input->post('motivation'),
			'createdAt' => date('Y-m-d H:i:s')
			);
		$q = $this->db->insert($this->tbl, $data);
		if ($q){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function checkRegister(){
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');

		$this->db->where('email', $email);
		$this->db->or_where('phone', $phone);
		$q = $this->db->get($this->tbl);
		if($q->num_rows() > 0){
			return ['status'=>false, 'msg'=>'Email atau telephone sudah terdaftar'];
		}else{
			return ['status'=>true, 'msg'=>'Email atau telephone belum terdaftar'];
		}
	}
}
