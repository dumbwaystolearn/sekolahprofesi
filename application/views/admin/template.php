
<!DOCTYPE html>
<html lang="en">
  <?php $this->load->view('admin/head'); ?>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php
            $this->load->view('admin/sidebar/index');
            $this->load->view('admin/navbar');
        ?>
        <div class="right_col" role="main">
          <div class="">
            <?php  $this->load->view($url_content);  ?>
          </div>
        </div>
        <?php $this->load->view('admin/footerContent'); ?>
      </div>
    </div>
<?php
  $this->load->view('admin/foot');
  if(isset($ajaxs)){
    foreach ($ajaxs as $ajax) {
      $this->load->view($ajax['ajax'], $ajax);
    }
  }
?>




  </body>
</html>
