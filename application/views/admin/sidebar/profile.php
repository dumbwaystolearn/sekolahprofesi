
<!-- menu profile quick info -->
<div class="profile clearfix">
  <div class="profile_pic">
    <img src="<?= base_url('assets/img/webdev.png'); ?>" alt="..." class="img-circle profile_img">
  </div>
  <div class="profile_info">
    <span>Welcome,</span>
    <h2><?= $this->session->userdata('s_nm') ?></h2>
  </div>
</div>
<!-- /menu profile quick info -->
