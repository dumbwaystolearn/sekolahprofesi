<div class="container">
  <div class="registry">
    <div class="col-md-4 col-sm-4 col-xs-12 text-center" style="padding:0px;">
      <div class="charRegister"><img src="<?php echo base_url(); ?>/assets/img/characters/web-mobile-developer.png" class="profession" alt=""></div>
      <h3><strong>Full Stack Mobile Developer</strong></h3>
      <div class="subLogos">
        <div class="col-md-4 col-sm-4 col-xs-4 text-center" style="padding:0px;">
          <img src="<?php echo base_url(); ?>/assets/img/logos/nodejs.png" alt=""/>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 text-center" style="padding:0px;">
          <img src="<?php echo base_url(); ?>/assets/img/logos/react.png" alt=""/>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 text-center" style="padding:0px;">
          <img src="<?php echo base_url(); ?>/assets/img/logos/mongodb.png" alt=""/>
        </div>

      </div>
      <div class="subLogos">
        <div class="col-md-4 col-sm-4 col-xs-4 text-center" style="padding:0px;">
          <img src="<?php echo base_url(); ?>/assets/img/logos/android.png" alt=""/>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 text-center" style="padding:0px;">
          <img src="<?php echo base_url(); ?>/assets/img/logos/ios.png" alt=""/>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 text-center" style="padding:0px;">
          <img src="<?php echo base_url(); ?>/assets/img/logos/mysql.png" alt=""/>
        </div>
      </div>
      <p class="RegisterDetail">Cocok untuk Anda yang ingin mempelajari pembuatan aplikasi mobile baik iOS maupun Android dengan framework besutan Facebook bernama React Native.
        Anda juga akan dibimbing dalam pembuatan backend dengan framework NodeJS, database Mongo, dan MYSQL</p>
      </div>

      <div class="col-md-8 col-sm-8 col-xs-12">
        <div class="page-tittle text-center">
          <h1><?= $title ?></h1>
        </div>

        <!-- <p class="text-muted font-13 m-b-30"></p> -->
        <form id="demo-form" class="form-horizontal FormRegister" data-parsley-validate >

          <div class="apply-form-row apply-form-row-first"><label class=" col-md-3 col-sm-3 col-xs-12" for="choice_profetion">Pilihan Profesi <span class="required">*</span></label>
            <div class="apply-form-row-item">
              <select class="form-control" name="choice_profetion">
                <option value="full stack mobile developer">Full stack mobile developer</option>
                <option value="">- Pilih -</option>
              </select>
            </div>
          </div>
          <div class="apply-form-row">
            <label class=" col-md-3 col-sm-3 col-xs-12" for="class_schedule">Jadwal Kelas <span class="required">*</span></label>
            <div class="apply-form-row-item">
              <select class="form-control" name="class_schedule">
                <option value="1 Mei 2017 - 30 Juni 2017">1 Mei 2017 - 30 Juni 2017</option>
                <option value="">- Pilih -</option>
              </select>
            </div>
          </div>
          <div class="apply-form-row">
            <label class=" col-md-3 col-sm-3 col-xs-12" for="first_name">Nama Depan <span class="required">*</span></label>
            <div class="apply-form-row-item">
              <input type="text" id="first_name"name="first_name" placeholder="contoh : Ega" class="form-register" required="required">
            </div>
          </div>
          <div class="apply-form-row">
            <label class=" col-md-3 col-sm-3 col-xs-12" for="last_name">Nama Belakang <span class="required">*</span></label>
            <div class="apply-form-row-item">
              <input type="text" id="last_name"name="last_name" placeholder="contoh : Wachid" class="form-register" required="required">
            </div>
          </div>
          <div class="apply-form-row">
            <label class=" col-md-3 col-sm-3 col-xs-12" for="age">Usia <span class="required">*</span></label>
            <div class="apply-form-row-item">
              <input type="text" id="age"name="age" placeholder="contoh : 27 tahun" class="form-register" required="required">
            </div>
          </div>
          <div class="apply-form-row">
            <label class=" col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span></label>
            <div class="apply-form-row-item">
              <input type="email" id="email"name="email" placeholder="contoh : sekolahprofesi@mail.com" class="form-register" required="required">
            </div>
          </div>
          <div class="apply-form-row">
            <label class=" col-md-3 col-sm-3 col-xs-12" for="telephone">Telepon <span class="required">*</span></label>
            <div class="apply-form-row-item">
              <input type="tel" id="telephone"name="telephone" placeholder="contoh : 081123xxxxxx" class="form-register" required="required">
            </div>
          </div>
          <div class="apply-form-row">
            <label class=" col-md-3 col-sm-3 col-xs-12" for="referrer">Dari mana anda mengetahui sekolah profesi <span class="required">*</span></label>
            <div class="apply-form-row-item">
              <select class="form-control" name="referrer">
                <option value="">- Pilih -</option>
                <option value="Facebook">Facebook</option>
                <option value="Google">Google</option>
                <option value="Website">Website</option>
                <option value="Teman">Teman</option>
                <option value="Lainnya">Lainnya</option>
              </select>
            </div>
          </div>
          <div class="apply-form-row">
            <label class=" col-md-3 col-sm-3 col-xs-12" for="payment_plan">Rencana Pembayaran <span class="required">*</span></label>
            <div class="apply-form-row-item">
              <!-- <div class="col-md-6 text-center" > -->
                <label class="col-md-6 col-sm-6 col-xs-12 text-center tunaiRegister">
                  <input type="radio" checked="" value="tunai" id="" <?php if($plan == 'tunai'){echo ' checked=""';} ?> name="payment_plan">
                  <h2><strong>Tunai</strong></h2>
                  <hr>
                  <ul class="priceList">
                    <li>dapatkan potongan 30% dengan membayar cash</li>
                    <li>seleksi dan wawancara</li>
                  </ul>
                </label>
              <!-- </div> -->

              <!-- <div class="col-md-6 text-center" style="padding:0 2px;"> -->
                <label class="col-md-6 col-sm-6 col-xs-12 text-center cicilanRegister">
                  <input type="radio" value="cicil" id="" <?php if($plan == 'cicil'){echo ' checked=""';} ?> name="payment_plan">
                  <h2><strong>Cicilan</strong></h2>
                  <hr/>
                  <h5>dibayar setelah lulus dan mendapat pekerjaan</h5>
                  <ul class="priceList">
                    <li>cicilan fleksibel dan tanpa bunga, maksimal 12 bulan</li>
                    <li>seleksi dan wawancara</li>
                  </ul>
                </label>
              <!-- </div> -->
            </div>
          </div>

          <div class="apply-form-row">
            <label class=" col-md-3 col-sm-3 col-xs-12" for="motivation">Motivasi anda mengikuti sekolah profesi</label>
            <div class="apply-form-row-item">
              <textarea id="motivation" name="motivation" class="textarea-register"></textarea>
            </div>
          </div>

          <div class="item">
            <label class="" for="option_register">Karena keterbatasan Penerimaan siswa SekolahProfesi Full Stack Mobile Development, silahkan pilih opsi berikut: <span class="required">*</span></label>
            <div class="radio">
              <label><input type="radio" checked=""value="Pre-Register" id="" name="option_register"> Saya ingin melakukan Pre-Registrasi IDR 200rb, untuk reservasi spot Saya dan mendapat T-Shirt Eksklusuf SekolahProfesi</label>
            </div>

            <div class="radio">
              <label><input type="radio"value="non Pre-Register" id="" name="option_register"> Saya mendaftar Tanpa Pre-Registrasi, dan saya setuju bila spot Saya bisa sewaktu-waktu diisi oleh siswa lain</label>
            </div>
          </div>

          <div class="registerChapcha">
            <div class="g-recaptcha" data-sitekey="6Le9ehkUAAAAAD45Cl2eYBKpL6UPa7dUgJFOKdpT"></div>
            <script src="https://www.google.com/recaptcha/api.js?hl=id" async defer></script>
          </div>

          <div class="ln_solid"></div>
          <div class="item text-center">
            <p class='info_opsi_register'>
              <?php
                if($plan == 'cicil'){
                  echo '<strong><span style="color:#6d6ba9;font-size:20px;">30jt</span> setelah lulus dan mendapat pekerjaan <br/>(max cicil 12 bulan)</strong>';
                }else{
                  echo '<strong>Diskon 30% <s style="color:#d66578"><span style="color:#6d6ba9;font-size:20px;">30jt</span></s> 21jt <br/>(+ diskon 10% jika Anda mendaftar sebelum 10 April 2017)</strong>';
                }
              ?>
            </p>
          </div>
          <div class="col-sm-12" id="label_res" ></div>
          <div class="form-group text-center">
            <!-- <div class="col-md-6 col-md-offset-3"> -->
            <div class="centerBtnRegister">
              <button id="apply"  class="btn btn-default">Apply</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
