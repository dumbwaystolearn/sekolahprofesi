<div class="container">
  <div class="registry">
  <div class="col-md-4 col-sm-4 col-xs-12 text-center" style="padding:0px;">
    <div class="charRegister"><img src="<?php echo base_url(); ?>/assets/img/webdev.png" class="profession" alt=""></div>
      <h3><strong>Full Stack Mobile Developer</strong></h3>
      <div class="subLogos">
        <div class="col-md-4 text-center" style="padding:0px;">
          <img src="<?php echo base_url(); ?>/assets/img/nodejs.png" alt=""/>

        </div>
        <div class="col-md-4 text-center" style="padding:0px;">
        <img src="<?php echo base_url(); ?>/assets/img/react.png" alt=""/>
      </div>
      <div class="col-md-4 text-center" style="padding:0px;">
        <img src="<?php echo base_url(); ?>/assets/img/mongo.png" alt=""/>
      </div>

      </div>
      <div class="subLogos">
        <div class="col-md-4 text-center" style="padding:0px;">
        <img src="<?php echo base_url(); ?>/assets/img/android.png" alt=""/>
      </div>
      <div class="col-md-4 text-center" style="padding:0px;">
        <img src="<?php echo base_url(); ?>/assets/img/ios.png" alt=""/>
      </div>
      <div class="col-md-4 text-center" style="padding:0px;">
        <img src="<?php echo base_url(); ?>/assets/img/mysql.png" alt=""/>
      </div>
      </div>
      <p class="RegisterDetail">Cocok untuk Anda yang ingin mempelajari pembuatan aplikasi mobile baik iOS maupun Android dengan framework besutan Facebook bernama React Native.
        Anda juga akan dibimbing dalam pembuatan backend dengan framework NodeJS, database Mongo, dan MYSQL</p>
    </div>

  <div class="col-md-8">
    <div class="page-tittle text-center">
        <h1><?= $title ?></h1>
    </div>

    <!-- <p class="text-muted font-13 m-b-30"></p> -->
      <form id="demo-form" class="form-horizontal FormRegister" data-parsley-validate >
        <div class="col-sm-12" <?php if(isset($lbl_status_result)){echo 'id="'.$lbl_status_result['id'].'"'; } ?> ></div>
        <?php
        // render input from controller
        $a = 0;
        foreach($input_form as $input){
          if($a == 0){
            echo '<div class="apply-form-row apply-form-row-first">';
          }else if($input['type-form'] == 'vertical'){
            echo '<div class="item">';
          }else{
            echo '<div class="apply-form-row">';
          }

          if(isset($input['type-form']) && $input['type-form'] == 'horizontal'){
            echo '<label class=" col-md-3 col-sm-3 col-xs-12" for="'.$input['name'].'">'
            .$input['label'];
            if($input['required']){echo ' <span class="required">*</span>';}
            echo '</label>
            <div class="apply-form-row-item">';
          }else{
            echo '<label class="" for="'.$input['name'].'">'
            .$input['label'].' <span class="required">*</span>
            </label>';
          }
          if($input['type'] == 'select'){
            $a = 0;
            echo '<select class="form-control" name="'.$input['name'].'">';
            foreach($input['data'] as $data){
              echo '<option value="'.$data['value'].'">'.$data['label'].'</option>';
              $a++;
            }
            echo '</select>';
          }else if($input['type'] == 'textarea'){
            // echo '<textarea id="'.$input['name'].'" name="'.$input['name'].'" class="form-control col-md-7 col-xs-12 clear"></textarea>';
            echo '<textarea id="'.$input['name'].'" name="'.$input['name'].'" class="'.$input['class'].'"></textarea>';
          }else if($input['type']=='radio'){
            $a = 0;
            foreach($input['data'] as $data){
              if(isset($input['custom']) && $input['custom']){
                echo '<div class="col-md-6 tunai text-center">
                  <h2><strong>Tunai</strong></h2>
                  <hr>
                  <input type="'.$input['type'].'"'; if($input['default'] == $a){echo ' checked=""';} echo 'value="'.$data['value'].'" id="" name="'.$input['name'].'"> '.$data['label'].'
                  <ul class="priceList">
                    <li>dapatkan potongan 30% dengan membayar cash</li>
                    <li>seleksi dan wawancara</li>
                  </ul>
                </div>';
              }else{
                echo '<div class="radio">
                <label>
                <input type="'.$input['type'].'"'; if($input['default'] == $a){echo ' checked=""';} echo 'value="'.$data['value'].'" id="" name="'.$input['name'].'"> '.$data['label'].'
                </label>
                </div>';
              }
              $a++;
            }
          }else{
            // echo '<input type="'.$input['type'].'" id="'.$input['name'].'" class="form-control col-md-7 col-xs-12 clear"';
            echo '<input type="'.$input['type'].'" id="'.$input['name'].'"';
            if(isset($input['rage'])){
              echo 'data-validate-length-range="'.$input['range'].'"';
            };
            echo 'name="'.$input['name'].'" placeholder="'.$input['placeholder'].'" class="'.$input['class'].'" required="required">';
          }
          if(isset($input['type-form']) && $input['type-form'] == 'horizontal'){
            echo '</div>';
          }
          echo '</div>';
        }
        // end render form controller

        //render recaptcha
        if(isset($recaptcha)){
          echo '<div class="registerChapcha">';
          echo $recaptcha;
          echo '</div>';
        }
        ?>

        <div class="ln_solid"></div>
        <div><p class='info_opsi_register'></p></div>
        <div class="form-group text-center">
          <!-- <div class="col-md-6 col-md-offset-3"> -->
          <div class="centerBtnRegister">
            <?php
            // render button
            foreach($btn_form as $btn){
              echo '<button id="'.$btn['id'].'"  class="'.$btn['class'].'">'.$btn['label'].'</button>';
            }
            // end render button
            ?>
          </div>
        </div>
      </form>
    </div>
  </div>
  </div>
</div>
