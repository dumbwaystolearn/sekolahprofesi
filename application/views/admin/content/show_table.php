
<div class="page-title">
  <div class="title_left">
    <h3><?= $title ?> <small></small></h3>
  </div>
</div>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Data <small>Request Silabus</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>

        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <p class="text-muted font-13 m-b-30">
          <?= $description ?>
        </p>
        <div id="lbl_res"></div>
        <table id="show-table" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" >
          <thead>
            <tr>
              <?php
                foreach($head_table as $head){
                  echo '<td>'.$head.'</td>';
                }
                if($btn_ud){
                  echo '<th></th>';
                }
              ?>
            </tr>
          </thead>

          <tbody>
            <?php
                foreach ($list_request as $data){
                  echo '<tr>';
                  foreach ($body_table as $col) {
                    echo '<td>';
                    $type = $col['type']; $name = $col['name']; $id=$data['id'];
                    if($type == 'label'){
                      echo $data[$name];
                    }else if($type == 'select'){
                      echo "<select class='table-select' id='$id' name='$name'>";
                      foreach($col['data'] as $opt){
                        $label = $opt['label']; $value = $opt['value'];
                        echo "<option value='$value'"; if($value == $data[$name]){echo "selected";}
                        echo ">".$label."</option>";
                      }
                      echo "</select>";
                    }
                    echo '</td>';
                  }
                  if($btn_ud){
                    echo '<td>
                           <button type="button" class="btn btn-warning btn-xs btn_edit" value="'.$data['id'].'">Edit</button>
                           <button type="button" class="btn btn-danger btn-xs btn_edit" value="'.$data['id'].'">Delete</button>
                          </td></tr>';
                  }
                }
             ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
    var table = $('#show-table').DataTable();
    $('.table-select').on('change', function() {
        console.log('on change table-select', this.value, this.id);
        $.ajax({
  				url: "<?php if(isset($url_ajax)){ echo $url_ajax;} ?>",
  				type: "POST", dataType: "JSON", data: {id: this.id, status:this.value},
  				error: function(xhr, err){
            console.log('error submit',xhr.responseText, err);
            $("#lbl_res").html("<div class='alert alert-danger'>Change Status Error</div>");
          },
  				success: function(result) {
            var html = '';
            if(result.status){
              html = '<div class="alert alert-success">'+result.msg+'</div>';
            }else{
              html = '<div class="alert alert-danger">'+result.msg+'</div>';
            }
            $("#lbl_res").html(html);
  				}
  			});
    });
} );
</script>
