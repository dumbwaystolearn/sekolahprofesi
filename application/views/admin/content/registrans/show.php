
<div class="page-title">
  <div class="title_left">
    <h3>Registrans <small></small></h3>
  </div>
</div>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Data <small>Request Silabus</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>

        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <p class="text-muted font-13 m-b-30">
          Data Request Silabus is data to show user was request silabus in Sekolah Profesi
        </p>
        <table id="datatable" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>Email</th>
              <th>Telephone</th>
              <th>Request At</th>
              <th></th>
            </tr>
          </thead>

          <tbody>
            <?php
                foreach ($list_request as $data){
                    echo '<tr>
                             <td>'.$data['id'].'</td>
                             <td>'.$data['name'].'</td>
                             <td>'.$data['email'].'</td>
                             <td>'.$data['telephone'].'</td>
                             <td>'.$data['createdAt'].'</td>
                             <td>
                              <button type="button" class="btn btn-warning btn-xs btn_edit" value="'.$data['id'].'">Edit</button>
                              <button type="button" class="btn btn-danger btn-xs btn_edit" value="'.$data['id'].'">Delete</button>
                             </td></tr>';
                }
             ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
