
<div class="page-title">
  <div class="title_left">
    <h3><?= $title ?> <small></small></h3>
  </div>
</div>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <!-- <h2>Data <small>Request Silabus</small></h2> -->
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>

        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <p class="text-muted font-13 m-b-30">
          <?= $description ?>
        </p>

        <form id="demo-form" class="form-horizontal form-label-left" data-parsley-validate >
          <div class="col-sm-12" <?php if(isset($lbl_status_result)){echo 'id="'.$lbl_status_result['id'].'"'; } ?> ></div>
          <?php
              // render input from controller
              foreach($input_form as $input){
                echo '<div class="item form-group">';
                if(isset($input['type-form']) && $input['type-form'] == 'horizontal'){
                  echo '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="'.$input['name'].'">'
                          .$input['label'];
                          if($input['required']){echo ' <span class="required">*</span>';}
                  echo '</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">';
                }else{
                  echo '<label class="control-label" for="'.$input['name'].'">'
                          .$input['label'].' <span class="required">*</span>
                        </label>';
                }
                if($input['type'] == 'select'){
                  $a = 0;
                  echo '<select class="form-control" name="'.$input['name'].'">';
                    foreach($input['data'] as $data){
                      echo '<option value="'.$data['value'].'">'.$data['label'].'</option>';
                      $a++;
                    }
                    echo '</select>';
                }else if($input['type'] == 'textarea'){
                  echo '<textarea id="'.$input['name'].'" name="'.$input['name'].'" class="form-control col-md-7 col-xs-12"></textarea>';
                }else if($input['type']=='radio'){
                  $a = 0;
                  foreach($input['data'] as $data){
                    echo '<div class="radio">
                              <label>
                                <input type="'.$input['type'].'"'; if($input['default'] == $a){echo ' checked=""';} echo 'value="'.$data['value'].'" id="" name="'.$input['name'].'"> '.$data['label'].'
                              </label>
                            </div>';
                            $a++;
                  }
                }else{
                  echo '<input type="'.$input['type'].'" id="'.$input['name'].'" class="form-control col-md-7 col-xs-12"';
                  if(isset($input['rage'])){
                    echo 'data-validate-length-range="'.$input['range'].'"';
                  }
                  echo 'name="'.$input['name'].'" placeholder="'.$input['placeholder'].'" required="required">';
                }
                if(isset($input['type-form']) && $input['type-form'] == 'horizontal'){
                  echo '</div>';
                }
                echo '</div>';
              }
              // end render form controller

              //render recaptcha
              if(isset($recaptcha)){
                echo $recaptcha;
              }
           ?>

            <div class="ln_solid"></div>
            <div>
              <p class='info_opsi_register'></p>
            </div>
            <div class="form-group">
              <div class="col-md-6 col-md-offset-3">
                <?php
                  // render button
                    foreach($btn_form as $btn){
                      echo '<button id="'.$btn['id'].'"  class="'.$btn['class'].'">'.$btn['label'].'</button>';
                    }
                    // end render button
                 ?>
              </div>
            </div>
          </form>

      </div>
    </div>
  </div>
</div>
