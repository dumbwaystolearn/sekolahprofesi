<script>
  $(document).ready(function () {
      $( "form" ).on( "submit", function( event, t ) {
        event.preventDefault();
        $.ajax({
  				url: "<?= $url ?>",
  				type: "POST",
  				dataType: "JSON",
  				data: $( this ).serialize(),
  				beforeSend: function() {
            $("#<?= $id_btn ?>").prop('disabled', true);
  					$("#<?= $id_btn ?>").html("<i class='fa fa-spinner fa-spin'></i> Submit...");
  				},
  				complete: function() {
  					$("#<?= $id_btn ?>").html("Submit");
            $("#<?= $id_btn ?>").prop('disabled', false);
  				},
          error: function(xhr, err){
            console.log('error submit',xhr.responseText, err);
            $("#<?= $id_lbl_res ?>").html("<div class='alert alert-danger'>Submit Error</div>");
            $("#<?= $id_btn ?>").prop('disabled', false);
            grecaptcha.reset();
          },
  				success: function(result) {
            var html = '';
            if(result.status){
              window.location.href = result.url;
              html = '<div class="alert alert-success">'+result.msg+'</div>';
              $('.clear').val('');
            }else{
              html = '<div class="alert alert-danger">'+result.msg+'</div>';
            }
            $("#<?= $id_lbl_res ?>").html(html);
            grecaptcha.reset();
  				}
  			});
      });
    });
</script>
