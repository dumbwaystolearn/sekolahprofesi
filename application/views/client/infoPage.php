
<!DOCTYPE html>
<html lang="en">
  <?php $this->load->view('client/head'); ?>
<body>
  <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand page-scroll" href="<?php echo base_url('#page-top')?>"><img src="<?php echo base_url('assets/img/main-logo.png');?>" class="main-logo" alt=""/></a>
    </div>
    <div class="navbar-collapse collapse">
      <!-- <ul class="nav navbar-nav">
      <li><a href="#">Left</a></li>
    </ul> -->
    <ul class="nav navbar-nav navbar-center">
      <li class="active"><a class="page-scroll" href="<?php echo base_url('#Home'); ?>"><string>Beranda</string></a></li>
      <li><a class="page-scroll" href='<?php echo base_url('#why');?>'><string>Mengapa SekolahProfesi</string></a></li>
      <li><a class="page-scroll" href='<?php echo base_url('#testimoni');?>'><string>Testimoni</string></a></li>
      <li><a class="page-scroll" href='<?php echo base_url('#team');?>'><string>Team</string></a></li>
      <li><a class="page-scroll" href='<?php echo base_url('#pricing');?>'><string>Pricing</string></a></li>
      <li><a class="page-scroll" href='<?php echo base_url('#contact');?>'><string>Kontak</string></a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a onClick="openInNewTab('<?php echo base_url('daftar');?>')" class="btn btn-primary btn-lg"> Daftar Sekarang </a></li>
    </ul>
  </div>
</div>
</nav>

  <div class="col-sm-8 text-center" style="position: absolute;
    top: 50%;
    left: 50%;
    transform: translateX(-50%) translateY(-50%);">
    <div class="jumbotron" style="background-color: #e65b6c;color: #FFF;">
      <div class="container">
        <h1>Pendaftaran Berhasil</h1>
        <p>Terimakasih telah mendaftar program Full Stack Mobile Developer di SekolahProfesi. Silakan cek email Anda untuk pemberitahuan lebih lanjut.</p>
        <p><a class="btn fb-share-button btn-lg" data-href="https://sekolahprofesi.id" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fsekolahprofesi.id%2F&amp;src=sdkpreparse"></a></p>
      </div>
    </div>
  </div>
  <div id="fb-root"></div>
  <script>
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  </script>
</body>

<script>

</script>
  </body>
</html>
