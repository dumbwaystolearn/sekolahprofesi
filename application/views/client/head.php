<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="SekolahProfesi"
  <meta name="description" content="SekolahProfesi adalah tempat yang tepat
  untuk anda yang ingin mewujudkan masa depan dan karir anda secara profesional.
  pengajaran Efisien & Menyenangkan bagi setiap peserta, terdapat jaminan kerja dari kami, juga Mentor Berpengalaman
  akan membantu anda menjadi profesional dalam profesi anda.
  Bagi Anda yang ingin menjalankan startup, kami juga menyediakan tempat diskusi dan mentoring untuk mensukseskan
  startup yang akan Anda buat.
  " />

  <meta name="keywords" content="SekolahProfesi, sekolahprofesi, sekolah profesi, sekolah coding, sekolah game,
  sekolah magic, sekolah IT, Sekolah profesi Indonesia" />


  <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>/assets/img/favicon.png"/>

  <title>SekolahProfesi | Wujudkan Profesi Impianmu Bersama Kami dalam 9 Minggu!</title>

    <link href="<?php echo base_url(); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/font-awesome/css/font-awesome.min.css">
    <link href="<?php echo base_url(); ?>/assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>/assets/css/responsive.css" rel="stylesheet">

    <!-- <link href="https://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css" rel="stylesheet"/> -->

</head>
