
<!DOCTYPE html>
<html lang="en">
  <?php $this->load->view('client/head'); ?>
  <link href="assets/silabus/SekolahProfesi-Silabus-Full-StackMobileDeveloper.pdf"/>
  <!-- <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <div class="right_col" role="main">
          <div class="">
            <?php //$this->load->view($url_content);  ?>
          </div>
        </div>
      </div>
    </div> -->

    <body id="page-top">
        <nav class="navbar navbar-default navbar-fixed-top affix">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand page-scroll" href="#page-top"><img src="assets/img/sekolahprofesi-logo-small.png" class="main-logo" alt=""/></a>
            </div>
            <div class="navbar-collapse collapse">
              <!-- <ul class="nav navbar-nav">
              <li><a href="#">Left</a></li>
            </ul> -->
            <ul class="nav navbar-nav navbar-center">
              <li class="active"><a class="page-scroll" href="#home"><string>Beranda</string></a></li>
              <li><a class="page-scroll" href='<?php echo base_url(); ?>#why'><string>Mengapa SekolahProfesi</string></a></li>
              <li><a class="page-scroll" href='<?php echo base_url(); ?>#testimoni'><string>Testimoni</string></a></li>
              <li><a class="page-scroll" href='<?php echo base_url(); ?>#team'><string>Team</string></a></li>
              <li><a class="page-scroll" href='<?php echo base_url(); ?>#pricing'><string>Pricing</string></a></li>
              <li><a class="page-scroll" href='<?php echo base_url(); ?>#contact'><string>Kontak</string></a></li>
            </ul>
          </div>
        </div>
      </nav>

      <?php $this->load->view($url_content);  ?>

<?php
  $this->load->view('client/foot');
  if(isset($ajaxs)){
    foreach ($ajaxs as $ajax) {
      $this->load->view($ajax['ajax'], $ajax);
    }
  }
?>
<script>
$(document).ready(function () {
  $('input[type=radio][name=payment_plan]').change(function() {
      var msg = '';
      if (this.value == 'tunai') {
        msg = '<strong>Diskon 30% IDR <s style="color:#d66578"><span style="color:#6d6ba9;font-size:20px;"> 30jt </span></s> <span style="color:#6d6ba9;font-size:25px;"> 21jt </span><br/>(+ diskon 10% jika Anda mendaftar sebelum 10 April 2017)</strong>';
      }else if (this.value == 'cicil') {
        msg = '<strong>IDR <span style="color:#6d6ba9;font-size:20px;"> 30jt </span>setelah lulus dan mendapat pekerjaan <br/>(max cicil 12 bulan)</strong>';
      }
      $('.info_opsi_register').html(msg);
  });
  // $('input[name=payment_plan]').change(function(){
  //   var payment_plan = ('select[name="payment_plan"]').val();
  //   var msg = '';
  //   if(payment_plan == 'tunai'){
  //     msg = 'Diskon 30% 30jt[coret] 21jt (+ diskon 10% jika Anda mendaftar sebelum 10 April 2017)';
  //   }else{
  //     msg = '30jt setelah lulus dan mendapat pekerjaan (max cicil 12 bulan)';
  //   }
  //   $('.info_opsi_register').html(msg);
  // });
});
</script>
  </body>
</html>
