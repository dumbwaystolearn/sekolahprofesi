<?php

header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');

class Emails extends CI_Controller {

	function __construct(){
			parent::__construct();
			$this->load->library(array('session', 'recaptcha'));
			$this->load->model(['EmailSend_mdl', 'EmailSend_mdl']);
	}

	function canSendEmail(){
		if(isset($_SESSION['countSend'])){
			$count = $_SESSION['countSend'];
		}else{
			$count =0;
		}
		$count++;
		$this->session->set_userdata('countSend', $count);
		if($count > 3){
			$this->session->mark_as_temp('countSend', 180);
			return ['status' => false, 'msg'=>'Mohon tunggu selama 3 menit untuk mengirim ulang.'];
		}
		return ['status'=>true, 'msg'=>'Email terkirim.'];
	}

	function regReCaptha(){
		$captcha_answer = $this->input->post('g-recaptcha-response');
		$response = $this->recaptcha->verifyResponse($captcha_answer);
		return ['status'=> $response['success'], 'msg'=>'recaptcha belum terisi', 'input'=>$this->input->post()];
	}

	function validasiInput(){
		$reCaptcha = $this->regReCaptha();
		if($reCaptcha['status']){
			$arInput = $this->input->post();
			foreach($arInput as $key => $value){
				if($key == 'g-recaptcha-response' && empty($value)){
					return ['status'=>false, 'msg'=>'Captcha tidak benar'];
				}else if($key == 'email' && !filter_var($value, FILTER_VALIDATE_EMAIL) ){
					return ['status'=>false, 'msg'=>'Format '.$key.' ('.$value.') tidak benar.'];
				}else if($key == 'name' && (trim($value) == '' || !preg_match('/^[a-zA-Z ]*$/',$value))){
					return ['status'=>false, 'msg'=>'Format '.$key.' ('.$value.') tidak benar.'];
				}else	if($key == 'telephone' && !preg_match('/^\(?\+?([0-9]{1,4})\)?[-\. ]?(\d{3})[-\. ]?([0-9]{7})$/', $value)){
					return ['status'=>false, 'msg'=>'Format '.$key.' ('.$value.') tidak benar'];
				}
			}
			return ['status'=>true, 'msg'=>'input terisi'];
		}else{
			return $reCaptcha;
		}
	}
	public function sendToApplicant($email_aplicant){
			$config_email = $this->config->item('config_email');
			$this->load->library('email', $config_email);
			$this->email->set_newline("\r\n");
			$this->email->from('pendaftar@gmail.com', 'register@sekolahprofesi.id');
			$this->email->to($email_aplicant);
			$this->email->attach(base_url().'/assets/silabus/silabus_sekolahProfesi.jpg');
			$this->email->subject('Silabus SekolahProfesi');

			$message = "
					Dear <b>".$this->input->post('name')."</b>,<br/><br/>

					Terima kasih telah melakukan request silabus SekolahProfesi. Silabus
					kita sertakan via attachment. Jika Anda berminat untuk mendaftar SekolahProfesi,
					silahkan kunjungi link berikut <a href='https://sekolahprofesi.id/daftar'>daftar SekolahProfesi</a> .<br/><br/>

					Dapatkan diskon 10% dengan mendaftar sebelum 10 April 2017. Lakukan juga Pre-registrasi dan dapatkan T-Shirt Eksklusif SekolahProfesi.
					<br/><br/>
					Regards,<br/>
					Team Sekolah Profesi";
			$this->email->message($message);
			$result = $this->email->send();
			$this->email->clear(TRUE);
			return $result;
	}

	public function sendToSekolah(){
			$config_email = $this->config->item('config_email');
			$this->load->library('email', $config_email);
			$this->email->set_newline("\r\n");
			$this->email->from('pendaftar@gmail.com', 'register@sekolahprofesi.id');
			$list_recipients = array('radiegtya@gmail.com', 'info@sekolahprofesi.id', 'me@mhaidarhanif.com');
			// $list_recipients = array('mucasalii@gmail.com');
			$this->email->to($list_recipients);
			$this->email->subject('Notifikasi Silabus SekolahProfesi');
			$message = "<ul><li>Name : ".$this->input->post('name')."</li>"
									."<li>Email : ".$this->input->post('email')."</li>"
									."<li>Telephone : ".$this->input->post('telephone')."</li>";
			$this->email->message($message);
			$result = $this->email->send();
			return $result;
	}

	public function sendEmail(){
		$valInput = $this->validasiInput();
		if($valInput['status'] == false){
			$validate = $valInput;
		}else{
			$validate = $this->EmailSend_mdl->checkSubmit();
		}
		if($validate['status']){
			$validate = $this->canSendEmail();
		}
		if($validate['status']){
			$emailApplicant = $this->input->post('email');
			$result1 = $this->sendToApplicant($emailApplicant);
			$result2 = $this->sendToSekolah();
			// $result1 =true; $result2 = true;
			if($result1 && $result2){
					$this->EmailSend_mdl->insert();
					$ar_result = ['status'=>$result1, 'msg'=>'Terima kasih telah mendaftar, kami akan segera menghubungi Anda.'];
				}else{
					$ar_result = ['status'=>$result1, 'msg'=>'Terjadi kesalahan pada pendaftaran.'];
				}
		}else{
			$ar_result = $validate;
		}
		echo json_encode($ar_result);
	}
}
