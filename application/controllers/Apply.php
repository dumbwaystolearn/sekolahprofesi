<?php

header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');

class Apply extends CI_Controller {

	function __construct(){
			parent::__construct();
			$this->load->library(array('session', 'recaptcha', 'validasi'));
			$this->load->model('Registrans_mdl');
	}

	 function index(){
		$plan = $this->input->get('plan');
		$d['plan'] = $plan;
		$d['title'] = 'Form Registrasi Full Stack Mobile Developer';
		$d['description'] = '';
		$d['url_content'] = 'admin/content/form';
		$d['ajaxs'] = array(
			['id_btn'=>'apply', 'id_lbl_res'=>'label_res', 'ajax'=>'js/ajaxInput', 'url'=>base_url("Apply/create")],
		);
		$d['recaptcha'] = $this->recaptcha->render();
		$d['lbl_status_result'] = ['id'=>'label_res'];
		$this->load->view('client/apply', $d);
	}

	function canSendEmail(){
		if(isset($_SESSION['countApply'])){
			$count = $_SESSION['countApply'];
		}else{
			$count =0;
		}
		$count++;
		$this->session->set_userdata('countApply', $count);
		if($count > 3){
			$this->session->mark_as_temp('countApply', 1);
			return ['status' => false, 'msg'=>'Mohon tunggu selama 3 menit untuk mengirim ulang.'];
		}
		return ['status'=>true, 'msg'=>'Email terkirim.'];
	}

	function regReCaptha(){
		$captcha_answer = $this->input->post('g-recaptcha-response');
		$response = $this->recaptcha->verifyResponse($captcha_answer);
		return ['status'=> $response['success'], 'msg'=>'recaptcha belum terisi', 'input'=>$this->input->post()];
	}

	function input_form($plan){
		if(!isset($plan) || $plan == 'tunai'){
			$plan = 0;
		}else{
			$plan = 1;
		}
		$referrers = array(
			['label'=>'-Pilih-', 'value'=>''],
			['label'=>'Facebook', 'value'=>'Facebook'],
			['label'=>'Google', 'value'=>'Google'],
			['label'=>'Website', 'value'=>'Website'],
			['label'=>'Teman', 'value'=>'Teman'],
			['label'=>'Lainnya', 'value'=>'Lainnya']
		);
		$payentPlan = array(
			['label'=>'Tunai', 'value'=>'tunai'],
			['label'=>'Bayar Setelah Bekerja', 'value'=>'cicil']
		);
		$class_schedule = array(
			['label'=>'-Pilih-', 'value'=>''],
			['label'=>'1 Mei 2017 - 30 Juni 2017', 'value'=>'1 Mei 2017 - 30 Juni 2017']
		);
		$option_register = array(
			['label'=>'Saya ingin melakukan Pre-Registrasi IDR 200rb, untuk reservasi spot Saya dan mendapat T-Shirt Eksklusuf SekolahProfesi', 'value'=>'Pre-Register'],
			['label'=>'Saya mendaftar Tanpa Pre-Registrasi, dan saya setuju bila spot Saya bisa sewaktu-waktu diisi oleh siswa lain', 'value'=>'non Pre-Register']
		);
		$choice_profetion = array(
			['label'=>'-Pilih-', 'value'=>''],
			['label'=>'Full stack mobile developer', 'value'=>'full stack mobile developer'],
		);
		return array(
			array('label'=>'Pilihan Profesi', 'name'=>'choice_profetion', 'type'=>'select', 'default'=>'pilih', 'data'=>$choice_profetion, 'type-form'=>'horizontal', 'required'=>true),
			array('label'=>'Jadwal Kelas', 'name'=>'class_schedule', 'type'=>'select', 'default'=>0, 'data'=>$class_schedule, 'type-form'=>'horizontal', 'required'=>true),
			array('label'=>'Nama Depan', 'name'=>'first_name', 'class'=>'form-register', 'type'=>'text', 'placeholder'=>'Ega', 'range'=>'1,100', 'type-form'=>'horizontal', 'required'=>true),
			array('label'=>'Nama Belakang', 'name'=>'last_name', 'class'=>'form-register', 'type'=>'text', 'placeholder'=>'Wachid', 'range'=>'1,100', 'type-form'=>'horizontal', 'required'=>true),
			array('label'=>'Usia', 'name'=>'age', 'type'=>'text', 'class'=>'form-register', 'placeholder'=>'27', 'range'=>'1,3', 'type-form'=>'horizontal', 'required'=>true),
			array('label'=>'Email', 'name'=>'email', 'type'=>'email', 'class'=>'form-register', 'placeholder'=>'sekolahprofesi@mail.com', 'type-form'=>'horizontal', 'required'=>true),
			array('label'=>'Telepon', 'name'=>'telephone', 'type'=>'tel', 'class'=>'form-register', 'placeholder'=>'085641085805', 'range'=>'8,13', 'type-form'=>'horizontal', 'required'=>true),
			array('label'=>'Dari mana anda mengetahui sekolah profesi', 'class'=>'form-register', 'name'=>'referrer', 'type'=>'select', 'default'=>1,'data'=> $referrers, 'type-form'=>'horizontal', 'required'=>true),
			array('label'=>'Rencana Pembayaran', 'name'=>'payment_plan', 'type'=>'radio', 'default'=>$plan, 'data'=>$payentPlan, 'type-form'=>'horizontal', 'required'=>true, 'custom'=>true),
			array('label'=>'Motivasi anda mengikuti sekolah profesi', 'class'=>'textarea-register', 'name'=>'motivation', 'type'=>'textarea', 'placeholder'=>'', 'type-form'=>'horizontal', 'required'=>false),
			array('label'=>'Karena keterbatasan Penerimaan siswa SekolahProfesi Full Stack Mobile Development, silahkan pilih opsi berikut:',
						'name'=>'option_register', 'type'=>'radio', 'default'=>0, 'data'=>$option_register, 'required'=>true, 'type-form'=>'vertical',
						)
			);
	}

	function validate(){
		$template_form = $this->input_form('tunai');
		$input_value = $this->input->post();
		$valInput = $this->validasi->validasiInput2($input_value, $template_form);
		if($valInput['status'] == false){
			$validate = $valInput;
		}else{
			$validate = $this->Registrans_mdl->checkRegistrant();
			if($validate['status']){
				$validate = $this->canSendEmail();
			}
		}
		return $validate;
	}

	public function create(){
		$ar_result = $this->validate();
		if($ar_result['status']){
			$emailApplicant = $this->input->post('email');
			$result1 = $this->sendToApplicant($emailApplicant);
			$result2 = $this->sendToSekolah();
			// $result1 =true; $result2 = true;
			if($result1 && $result2){
				$this->Registrans_mdl->insert();
				$ar_result = ['status'=>$result1, 'msg'=>'Terima kasih telah mendaftar, silahkan cek email anda untuk pemberitahuan lebih lanjut.', 'url'=>base_url('daftar/sukses')];
			}else{
				$ar_result = ['status'=>$result1.' - '.$result2, 'msg'=>'Terjadi kesalahan pada pendaftaran.'];

			}
		}
		echo json_encode($ar_result);
	}

	public function infoPage(){
		$this->load->view('client/infoPage');
	}

	public function sendToApplicant($email_aplicant){
			$config_email = $this->config->item('config_email');
			$this->load->library('email', $config_email);
			$this->email->set_newline("\r\n");
			$this->email->from('pendaftar@gmail.com', 'register@sekolahprofesi.id');
			$this->email->to($email_aplicant);
      $this->email->attach(base_url().'/assets/silabus/silabus_sekolahProfesi.jpg');
			$this->email->subject('Registrasi SekolahProfesi');

			$message = "
					Dear <b>".$this->input->post('first_name').' '.$this->input->post('last_name')."</b>,<br/><br/>

					Terima kasih telah melakukan pendaftaran di SekolahProfesi dengan diskon 10%. Reservasi spot kamu segera dengan IDR 200rb!<br/><br/>

					Lakukan pembayaran melalui bank kami :<br/>
					BCA 320 040 1823 a.n Ega Wachid<br/>
					Mandiri 900 002 773 5407 a.n Ega Wachid<br/>
					<br/>
					Konfirmasi via WA/Telp +6285641278479.<br/>
					<br/>

					Regards,<br/>
					Team Sekolah Profesi<br/>
					[Simpan Email ini sebagai bukti diskon]
			";
			$this->email->message($message);
			$result = $this->email->send();
			$this->email->clear(TRUE);
			return $result;
	}

	public function sendToSekolah(){
			$config_email = $this->config->item('config_email');
			$this->load->library('email', $config_email);
			$this->email->set_newline("\r\n");
			$this->email->from('pendaftar@gmail.com', 'register@sekolahprofesi.id');
			$list_recipients = array('radiegtya@gmail.com', 'info@sekolahprofesi.id');
			// $list_recipients = array('mucasalii@gmail.com');
			$this->email->to($list_recipients);
			$this->email->subject('Notifikasi Registrasi SekolahProfesi');
			$message = "
						<ul><li>First Name : ".$this->input->post('first_name')."</li>"
					."<li>Last Name : ".$this->input->post('last_name')."</li>"
					."<li>Age : ".$this->input->post('age')."</li>"
					."<li>Email : ".$this->input->post('email')."</li>"
					."<li>Telephone : ".$this->input->post('telephone')."</li>"
					."<li>Payment Plan : ".$this->input->post('payment_plan')."</li>"
					."<li>Motivation : ".$this->input->post('motivation')."</li>"
					."<li>Class Schedule : ".$this->input->post('class_schedule')."</li>"
					."<li>Option Register : ".$this->input->post('option_register')."</li>"
					."<li>Choice Profetion : ".$this->input->post('choice_profetion')."</li>";
			$this->email->message($message);
			$result = $this->email->send();
			return $result;
	}

}
