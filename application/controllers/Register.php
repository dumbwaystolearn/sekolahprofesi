<?php

header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	function __construct(){
			parent::__construct();
			$this->load->library(array('session'));
			if (!$this->session->userdata('s_act')){
				redirect('logins');
			}
			$this->load->helper(array('captcha', 'url'));
	}

 public function index(){
        $vals = array(
	        'img_path'      => './captcha/',
	        'img_url'       => base_url().'captcha/',
	        'font_path'     => './path/to/fonts/texb.ttf',
	        'img_width'     => '150',
	        'img_height'    => 30,
	        'expiration'    => 7200,
	        'word_length'   => 8,
	        'font_size'     => 16,
	        'img_id'        => 'Imageid',
	        'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',

	        // White background and border, black text and red grid
	        'colors'        => array(
	                'background' => array(255, 255, 255),
	                'border' => array(0, 255, 0),
	                'text' => array(0, 0, 0),
	                'grid' => array(255, 40, 40)
	        )
      );

      $cap = create_captcha($vals);
      // echo 'captcha '.$cap['word'].'->'.$cap['image'];
			$this->load->library('recaptcha');
			echo $this->recaptcha->render();
  }

	public function reCaptcha(){
		$this->load->library('recaptcha');
		echo $this->recaptcha->render();
	}



}
