<?php
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');


class Login extends CI_Controller{
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if ($this->session->userdata('s_act')){
			redirect('admin');
		}
	}

	function index(){
		$d['title'] = 'Login';
		$this->load->view('admin/login', $d);
	}

	public function a(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('txt_uid', 'Email atau Username', 'required|max_length[40]|htmlspecialchars');
		$this->form_validation->set_rules('txt_upass', 'Password', 'required|htmlspecialchars');
		$this->form_validation->set_message('required', "%s Can't Null.");
		$this->form_validation->set_message('max_length', '%s maximum %s character.');
		echo 'hello ->'.$this->form_validation->run();
	}

	public function auth(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('txt_uid', 'Email atau Username', 'required|max_length[40]|htmlspecialchars');
		$this->form_validation->set_rules('txt_upass', 'Password', 'required|htmlspecialchars');
		$this->form_validation->set_message('required', "%s Can't Null.");
		$this->form_validation->set_message('max_length', '%s maximum %s character.');

		if ($this->form_validation->run() == FALSE){
			$d['title'] = 'Login - Gagal';
			$this->load->view('login', $d);
		}else{
			$this->load->model('users_mdl', 'user');
      $data = $this->user->getUserByUsername($this->input->post('txt_uid'));
			if ($data['status'] == TRUE){
        $d = $data['hsl'];
        if($d['password'] == md5($this->input->post('txt_upass'))){
					$sesi = array(
						's_act' => TRUE,
						's_id' => $d['id'],
						's_nm' => $d['name'],
						's_group' => $d['group'],
						's_un' => $d['username']
						);
					$this->session->set_userdata($sesi);
					redirect('admin');
	      }else{
          $d['pesan'] = 'Password Not Valid';
          $this->load->view('admin/login', $d);
	      }
			}else{
          $d['pesan'] = 'Email atau Username Not Found';
          $this->load->view('admin/login', $d);
			}
		}
	}

}
