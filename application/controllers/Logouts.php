<?php if ( ! defined('BASEPATH')) exit('Dilarang akses langsung gan?!');


class Logouts extends CI_Controller{
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}

	function index()
	{
		$this->session->sess_destroy();
		redirect('', 'refresh');
	}
}
