<?php

header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');

class RequestSilabus extends CI_Controller {

	function __construct(){
			parent::__construct();
			$this->load->library(array('session', 'recaptcha'));

			if (!$this->session->userdata('s_act')){
				redirect('logins');
			}
			$this->load->model('EmailSend_mdl');
	}

	public function index(){
		$q = $this->EmailSend_mdl->find();
		if ($q['status'] == TRUE)	{
			$d['list_request'] = $q['result'];
		}
		$d['title'] = 'Request Silabus';
		$d['description'] = 'Request Silabus for storing data Request Silabus';
		$d['btn_ud'] = false;
		$d['head_table'] = array('id', 'name', 'email','Telephone', 'Request At');
		$d['body_table'] = array(
			['type'=>'label', 'data'=>null, 'name'=>'id'],
			['type'=>'label', 'data'=>null, 'name'=>'name'],
			['type'=>'label', 'data'=>null, 'name'=>'email'],
			['type'=>'label', 'data'=>null, 'name'=>'telephone'],
			['type'=>'label', 'data'=>null, 'name'=>'createdAt']);
		$d['url_content'] = 'admin/content/show_table';
		$this->load->view('admin/template', $d);
	}

}
